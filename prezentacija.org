#+TITLE: Totalne funkcije: apstrakcijski alat u programiranju
#+AUTHOR: Marko Dimjašević
#+LOCATION: FER
#+DATE: IEEE, Hrvatska sekcija, Zagreb, 11.12.2018.
#+LANGUAGE: hr
#+OPTIONS: timestamp:nil toc:nil num:nil
#+REVEAL_THEME: solarized
#+REVEAL_TRANS: slides
#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/
#+REVEAL_PLUGINS: (highlight zoom)
#+REVEAL_MAX_SCALE: 3.0
#+ATTR_REVEAL: :frag grow
#+REVEAL_HLEVEL: 2

* Sažetak                                                          :noexport:

Apstrakcija je kamen temeljac u programiranju složenih softverskih
sustava. Bez apstrakcije, složeni softverski sustav je kompliciran softverski
sustav. U ovom predavanju će se razmatrati bitan apstrakcijski alat u
programiranju: totalne funkcije. Totalnost funkcija obuhvaća konačnost i
produktivnost. Ako očekivano konačna funkcija nije totalna, to može dovesti do
neregularnog završetka izvođenja programa ili može odvesti u beskonačnu
petlju. Nadalje, parcijalna funkcija koja se treba izvoditi bez prekida može
dovesti u potpuni zastoj. Ovo predavanje razmatra tehnike za postizanje
totalnosti funkcija u programiranju.

* O predavaču                                                      :noexport:

Marko Dimjašević je računalni znanstvenik s istraživačkim interesom u
područjima teorije tipova i formalnih metoda. Radi kao inženjer za formalne
metode na formalnom specificiranju blokovnih lanaca u tvrtci Input Output Hong
Kong. Diplomirao je računarstvo na Fakultetu elektrotehnike i računarstva u
Zagrebu. Doktorirao je računarstvo na Sveučilištu Utah u SAD-u na temu
automatskog ispitivanja softvera. U NASA-i je istraživao i razvijao tehnike za
ispitivanje i verificiranje softverskog sustava za izbjegavanje sudara
zrakoplova.

* Pregled predavanja

  - Uvod
  - Iscrpno podudaranje uzoraka
  - Konačnost
  - Produktivnost
  - Totalnost

* Uvod - apstrakcija

    “Svrha apstrakcije nije nejasnost, već stvaranje nove semantičke razine na
    kojoj je moguća apsolutna preciznost.”
      
      ~ Edsger Dijkstra

* Uvod - softverske komponente

  - Funkcija kao softverska komponenta
  - Korištenje manjih dijelova (komponenti) čiju funkcionalnost poznajemo
  - Spajanje manjih komponenti u veće
  - Apstrakcija nad detaljima manjih komponenti

* Uvod - Haskell i Idris

  - Čisti funkcijski programski jezici
  - Haskell nastao 1990., Idris 2009.
  - Tipski sustavi bazirani na parametarskom polimorfizmu
    - Algebarski tipovi podataka
    - Idris: ovisni tipovi
  - "Programiranje kao matematika"
  - Općenamjenski programski jezici
    - Idris dodatno asistent u dokazivanju teorema

* Uvod - Uloga tipova

  - Tip - skup vrijednosti
  - Funkcija - preslikavanje jednog skupa u drugi
  - Tipovi određuju vrstu podataka s kojima funkcije rade
    - Sužavanje prostora za pogreške
  - Kompilator: automatska provjera podudarnosti očekivanih i stvarnih tipova

* Iscrpno podudaranje uzoraka (1)

Dohvaćanje glave liste
#+BEGIN_SRC haskell
  head       :: [a] -> a
  head (x:_) =  x
#+END_SRC

* Iscrpno podudaranje uzoraka (2)

Pokrivanje cijele domene funkcije
#+BEGIN_SRC haskell
  head       :: [a] -> a
  head (x:_) =  x
  head []    =  error "empty list"
#+END_SRC

Greška pri izvođenju *head []*.

* Iscrpno podudaranje uzoraka (3)

Dobar odabir kodomene
#+BEGIN_SRC haskell
  head       :: [a] -> Maybe a
  head (x:_) =  Just x
  head []    =  Nothing
#+END_SRC

* Iscrpno podudaranje uzoraka (4)

  - Primjer: pozdrav vlasniku vozila
  - Usporedba u Haskellu i Idrisu
  - Korisnički tip podataka
    - Dob: punoljetnost kao uvjet posjedovanja
    - Vrsta vozila: motocikl ili automobil

* Iscrpno podudaranje uzoraka (5)

Haskell

#+BEGIN_SRC haskell
data Vehicle = Car | Motorcycle
data Person = MkPerson Int Vehicle

limit = 18

greet :: Person -> String
greet (MkPerson age _)          | age <  limit =
  "Be patient, you're not old enough to drive!"
greet (MkPerson age Car)        | age >= limit =
  "Hello, you car driver!"
greet (MkPerson age Motorcycle) | age >= limit =
  "Hello, you motorcycle driver!"
#+END_SRC

* Iscrpno podudaranje uzoraka (6)

#+BEGIN_SRC
$ ghc -Wincomplete-patterns Greet.hs
[1 of 1] Compiling Greet            ( Greet.hs, Greet.o )

Greet.hs:7:1: warning: [-Wincomplete-patterns]
    Pattern match(es) are non-exhaustive
    In an equation for ‘greet’:
	Patterns not matched:
	    (MkPerson _ Car)
	    (MkPerson _ Motorcycle)
   |
7  | greet (MkPerson age _)          | age <  limit =
   | ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^...
#+END_SRC

** Korišten GHC                                                    :noexport:
   - GHC 8.6.2, najnovija inačica kompilatora dostupna do 2. studenog 2018.

* Iscrpno podudaranje uzoraka (7)

Idris

#+BEGIN_EXPORT html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!-- Created by htmlize-1.51 in inline-css mode. -->
<html>
  <head>
    <title>Greet.idr</title>
  </head>
  <body style="color: #000000; background-color: #ffffff;">
    <pre>
<span style="color: #a020f0;">module</span> <span style="color: #000000; background-color: #ffffff; font-style: italic;">Greet</span>

<span style="color: #a020f0;">data</span> <span style="color: #0000ff; background-color: #ffffff;">Vehicle</span> <span style="color: #a0522d;">=</span> <span style="color: #ff0000; background-color: #ffffff;">Car</span> <span style="color: #a0522d;">|</span> <span style="color: #ff0000; background-color: #ffffff;">Motorcycle</span>

<span style="color: #006400; background-color: #ffffff;">limit</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span>
<span style="color: #006400; background-color: #ffffff;">limit</span> <span style="color: #a0522d;">=</span> <span style="color: #ff0000;">18</span>

<span style="color: #006400; background-color: #ffffff;">agePredicate</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Bool</span>
<span style="color: #006400; background-color: #ffffff;">agePredicate</span> <span style="color: #a020f0; background-color: #ffffff;">n</span> <span style="color: #a0522d;">=</span> <span style="color: #a020f0; background-color: #ffffff;">n</span> <span style="color: #006400;">&lt;</span> <span style="color: #006400; background-color: #ffffff;">limit</span>

<span style="color: #006400; background-color: #ffffff;">possiblyVehicle</span> <span style="color: #a0522d;">:</span> {<span style="color: #a020f0; background-color: #ffffff;">p</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Bool</span>} <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Type</span>
<span style="color: #006400; background-color: #ffffff;">possiblyVehicle</span> {<span style="color: #a020f0; background-color: #ffffff;">p</span>} <span style="color: #a020f0; background-color: #ffffff;">n</span> <span style="color: #a0522d;">=</span> <span style="color: #a020f0;">if</span> <span style="color: #a020f0; background-color: #ffffff;">p</span> <span style="color: #a020f0; background-color: #ffffff;">n</span> <span style="color: #a020f0;">then</span> <span style="color: #0000ff;">()</span> <span style="color: #a020f0;">else</span> <span style="color: #0000ff; background-color: #ffffff;">Vehicle</span>
</pre>
  </body>
</html>
#+END_EXPORT


* Iscrpno podudaranje uzoraka (8)

#+BEGIN_EXPORT html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!-- Created by htmlize-1.51 in inline-css mode. -->
<html>
  <head>
    <title>Greet.idr</title>
  </head>
  <body style="color: #000000; background-color: #ffffff;">
    <pre>
<span style="color: #a020f0;">data</span> <span style="color: #0000ff; background-color: #ffffff;">Person</span> <span style="color: #a0522d;">:</span> (<span style="color: #a020f0; background-color: #ffffff;">a</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span>) <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Bool</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Type</span> <span style="color: #a020f0;">where</span>
  <span style="color: #ff0000; background-color: #ffffff;">MkPerson</span>
    <span style="color: #a0522d;">:</span>  (<span style="color: #a020f0; background-color: #ffffff;">age</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span>)
    <span style="color: #a0522d;">-&gt;</span> (<span style="color: #a020f0; background-color: #ffffff;">v</span> <span style="color: #a0522d;">:</span> <span style="color: #006400; background-color: #ffffff;">possiblyVehicle</span> {<span style="color: #000000; background-color: #ffffff;">p</span> <span style="color: #a0522d;">=</span> <span style="color: #006400; background-color: #ffffff;">agePredicate</span>} <span style="color: #a020f0; background-color: #ffffff;">age</span>)
    <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Person</span> <span style="color: #a020f0; background-color: #ffffff;">age</span> (<span style="color: #006400; background-color: #ffffff;">agePredicate</span> <span style="color: #a020f0; background-color: #ffffff;">age</span>)

<span style="color: #006400; background-color: #ffffff;">greet</span> <span style="color: #a0522d;">:</span> {<span style="color: #a020f0; background-color: #ffffff;">age</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Nat</span>} <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">Person</span> <span style="color: #a020f0; background-color: #ffffff;">age</span> (<span style="color: #006400; background-color: #ffffff;">agePredicate</span> <span style="color: #a020f0; background-color: #ffffff;">age</span>) <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">String</span>
<span style="color: #006400; background-color: #ffffff;">greet</span> (<span style="color: #ff0000; background-color: #ffffff;">MkPerson</span> <span style="color: #a020f0; background-color: #ffffff;">age</span> <span style="color: #a020f0; background-color: #ffffff;">v</span>) <span style="color: #a020f0;">with</span> (<span style="color: #006400; background-color: #ffffff;">agePredicate</span> <span style="color: #a020f0; background-color: #ffffff;">age</span>)
  <span style="color: #006400; background-color: #ffffff;">greet</span> (<span style="color: #000000; background-color: #ffffff;">MkPerson</span> <span style="color: #000000; background-color: #ffffff;">_</span> <span style="color: #ff0000;">()</span>) <span style="color: #a0522d;">|</span> <span style="color: #ff0000; background-color: #ffffff;">True</span> <span style="color: #a0522d;">=</span>
    <span style="color: #ff0000;">"Be patient, you're not old enough to drive!"</span>
  <span style="color: #006400; background-color: #ffffff;">greet</span> (<span style="color: #000000; background-color: #ffffff;">MkPerson</span> <span style="color: #000000; background-color: #ffffff;">_</span> <span style="color: #ff0000; background-color: #ffffff;">Car</span>) <span style="color: #a0522d;">|</span> <span style="color: #ff0000; background-color: #ffffff;">False</span> <span style="color: #a0522d;">=</span>
    <span style="color: #ff0000;">"Hello, you car driver!"</span>
  <span style="color: #006400; background-color: #ffffff;">greet</span> (<span style="color: #000000; background-color: #ffffff;">MkPerson</span> <span style="color: #000000; background-color: #ffffff;">_</span> <span style="color: #ff0000; background-color: #ffffff;">Motorcycle</span>) <span style="color: #a0522d;">|</span> <span style="color: #ff0000; background-color: #ffffff;">False</span> <span style="color: #a0522d;">=</span>
    <span style="color: #ff0000;">"Hello, you motorcycle driver!"</span>
</pre>
  </body>
</html>
#+END_EXPORT

* Iscrpno podudaranje uzoraka (9)

  - Provjera iscrpnosti od strane Idrisovog kompilatora
    #+BEGIN_EXPORT html
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
    <!-- Created by htmlize-1.51 in inline-css mode. -->
    <html>
      <head>
        <title>*idris-repl*</title>
      </head>
      <body style="color: #000000; background-color: #ffffff;">
        <pre>
    <span style="color: #a020f0;">&#955;&#928;&gt; </span><span style="font-weight: bold;">:total greet</span>
    <span style="color: #006400;">Greet.greet</span> is Total
      </body>
    </html>
    #+END_EXPORT
  - Funkcija *greet* iscrpno pokriva sve moguće oblike i vrijednosti tipa
    *Person*

* Konačnost (1)

Primjer u Haskellu iz članka Total Functional Programming

#+BEGIN_SRC haskell
loop :: Int -> Int
loop n = 1 + loop n
#+END_SRC

* Konačnost (2)

  - Algebarsko rasuđivanje
  - Uvrstimo =n = 0=:
    #+BEGIN_SRC haskell
      loop 0 = 1 + loop 0
    #+END_SRC
  - Pretpostavimo =x - x = 0= i oduzmimo =loop 0= s obje strane:
    #+BEGIN_SRC haskell
      0 = 1
    #+END_SRC

* Konačnost (3)

  - Od programa

    #+BEGIN_SRC haskell
      loop :: Int -> Int
      loop n = 1 + loop n
    #+END_SRC

    dobivena tvrdnja

    #+BEGIN_SRC haskell
      0 = 1
    #+END_SRC

  - =n= nije samo tipa cijeli broj, nego i dno (nedefinirani cijeli broj)
  - Beskonačna petlja u programiranju odgovara laži u matematičkoj logici

* Konačnost (4): Curry-Howard

  - Curry-Howardova korespodencija
  - Poveznica između matematičke logike i programiranja
  - Tipovi su teoremi
  - Vrijednosti određenog tipa (programi) su dokazi pripadnih teorema
  - Izvođenje programa je pojednostavljenje dokaza

* Konačnost (5)

  - Neiscrpno podudaranje uzoraka i beskonačne petlje
    #+BEGIN_SRC haskell
      head       :: [a] -> a
      head (x:_) =  x

      loop :: Int -> Int
      loop n = 1 + loop n
    #+END_SRC

  - Oslanjanje na takve funkcije kasnije vodi u probleme: mukotrpno traženje i
    ispravljanje greški

* Konačnost: problem zaustavljanja

  - Problem zaustavljanja u teoriji izračunljivosti
    - Za dani program i ulaznu vrijednost, hoće li program završiti s
      izvođenjem?
    - Alan Turing 1936. dokazao da ne postoji općeniti algoritam koji rješava
      problem
  - Kako Idris može provjeriti konačnost funkcija?
    - Ograničenje: klasa funkcija za koju je to moguće

* Produktivnost

  - Što s programima za koje ne očekujemo da prestanu s izvođenjem,
    npr. operacijski sustav i poslužitelj weba?
    - Ovakvi programi stvaraju podatke za dani ulaz i tu radnju ponavljaju u
      petlji
  - Produktivnost: davanje nepraznog prvog dijela beskonačnog rezultata u
    konačnom vremenu

* Produktivnost: gorivo (1)

  - Potrošnja goriva kao garancija pomaka prema konačnom stanju u kojem nema
    goriva
  - Beskonačni spremnik goriva
    - Pomicanje beskonačnosti van ključnog dijela programa

* Produktivnost: gorivo (2)

  - Prilagođeni primjer iz knjige Type-driven Development with Idris

#+BEGIN_EXPORT html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!-- Created by htmlize-1.51 in inline-css mode. -->
<html>
  <head>
    <title>RunFuel.idr</title>
  </head>
  <body style="color: #000000; background-color: #ffffff;">
    <pre>
<span style="color: #a020f0;">module</span> <span style="color: #000000; background-color: #ffffff; font-style: italic;">RunFuel</span>

<span style="color: #a020f0;">%default</span> <span style="color: #a020f0;">total</span>

<span style="color: #a020f0;">data</span> <span style="color: #0000ff; background-color: #ffffff;">Fuel</span> <span style="color: #a0522d;">=</span> <span style="color: #ff0000; background-color: #ffffff;">Dry</span> <span style="color: #a0522d;">|</span> <span style="color: #ff0000; background-color: #ffffff;">More</span> (<span style="color: #006400; background-color: #ffffff;">Lazy</span> <span style="color: #0000ff; background-color: #ffffff;">Fuel</span>)

<span style="color: #a020f0;">partial</span>
<span style="color: #006400; background-color: #ffffff;">forever</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Fuel</span>
<span style="color: #006400; background-color: #ffffff;">forever</span> <span style="color: #a0522d;">=</span> <span style="color: #ff0000; background-color: #ffffff;">More</span> <span style="color: #006400; background-color: #ffffff;">forever</span>

<span style="color: #a020f0;">data</span> <span style="color: #0000ff; background-color: #ffffff;">InfIO</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Type</span> <span style="color: #a020f0;">where</span>
  <span style="color: #ff0000; background-color: #ffffff;">Do</span> <span style="color: #a0522d;">:</span> <span style="color: #006400; background-color: #ffffff;">IO</span> <span style="color: #a020f0; background-color: #ffffff;">a</span> <span style="color: #a0522d;">-&gt;</span> (<span style="color: #a020f0; background-color: #ffffff;">a</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #006400; background-color: #ffffff;">Inf</span> <span style="color: #0000ff; background-color: #ffffff;">InfIO</span>) <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">InfIO</span>
</pre>
  </body>
</html>
#+END_EXPORT

* Produktivnost: gorivo (3)

#+BEGIN_EXPORT html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!-- Created by htmlize-1.51 in inline-css mode. -->
<html>
  <head>
    <title>RunFuel.idr</title>
  </head>
  <body style="color: #000000; background-color: #ffffff;">
    <pre>
<span style="color: #006400; background-color: #ffffff;">infProg</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">InfIO</span>
<span style="color: #006400; background-color: #ffffff;">infProg</span> <span style="color: #a0522d;">=</span> <span style="color: #ff0000; background-color: #ffffff;">Do</span> (<span style="color: #006400; background-color: #ffffff;">putStrLn</span> <span style="color: #ff0000;">"Lambda"</span>) (<span style="color: #a0522d;">\</span><span style="color: #000000; background-color: #ffffff;">_</span> <span style="color: #a0522d;">=&gt;</span> <span style="color: #006400; background-color: #ffffff;">infProg</span>)

<span style="color: #006400; background-color: #ffffff;">run</span> <span style="color: #a0522d;">:</span> <span style="color: #0000ff; background-color: #ffffff;">Fuel</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #0000ff; background-color: #ffffff;">InfIO</span> <span style="color: #a0522d;">-&gt;</span> <span style="color: #006400; background-color: #ffffff;">IO</span> <span style="color: #0000ff;">()</span>
<span style="color: #006400; background-color: #ffffff;">run</span> (<span style="color: #ff0000; background-color: #ffffff;">More</span> <span style="color: #a020f0; background-color: #ffffff;">fuel</span>) (<span style="color: #ff0000; background-color: #ffffff;">Do</span> <span style="color: #a020f0; background-color: #ffffff;">c</span> <span style="color: #a020f0; background-color: #ffffff;">f</span>) <span style="color: #a0522d;">=</span> <span style="color: #a020f0;">do</span> <span style="color: #a020f0; background-color: #ffffff;">res</span> <span style="color: #a0522d;">&lt;-</span> <span style="color: #a020f0; background-color: #ffffff;">c</span>
                              <span style="color: #006400; background-color: #ffffff;">run</span> <span style="color: #a020f0; background-color: #ffffff;">fuel</span> (<span style="color: #a020f0; background-color: #ffffff;">f</span> <span style="color: #a020f0; background-color: #ffffff;">res</span>)
<span style="color: #006400; background-color: #ffffff;">run</span> <span style="color: #ff0000; background-color: #ffffff;">Dry</span> <span style="color: #000000; background-color: #ffffff;">_</span> <span style="color: #a0522d;">=</span> <span style="color: #006400; background-color: #ffffff;">putStrLn</span> <span style="color: #ff0000;">"Out of fuel"</span>

<span style="color: #a020f0;">partial</span>
<span style="color: #006400; background-color: #ffffff;">main</span> <span style="color: #a0522d;">:</span> <span style="color: #006400; background-color: #ffffff;">IO</span> <span style="color: #0000ff;">()</span>
<span style="color: #006400; background-color: #ffffff;">main</span> <span style="color: #a0522d;">=</span> <span style="color: #006400; background-color: #ffffff;">run</span> <span style="color: #006400; background-color: #ffffff;">forever</span> <span style="color: #006400; background-color: #ffffff;">infProg</span>
</pre>
  </body>
</html>
#+END_EXPORT

* Produktivnost - prednosti

  - Produktivni programi uvijek daju prvi dio rezultata u konačnom vremenu
  - Garancija da neće doći do zastoja u programima s istovremenskim izvođenjem
    više dretvi

* Totalnost (1)

  - Totalnost obuhvaća konačnost i produktivnost
  - Totalna funkcija je ona koja:
    a) završava s izvođenjem za vrijednost ispravnog tipa ili
    b) daje neprazni prvi dio rezultata ispravnog tipa u konačnom vremenu

* Totalnost (2)

  - Programi se mogu podijeliti na konačni i ne-konačni dio:
    - Konačni dio uvijek treba biti totalan
    - Ne-konačni dio treba u čim većem dijelu biti produktivan
      - Mogućnost greške tokom izvođenja samo u dijelu koji nije konačan i
        nije produktivan

* Totalnost (3)

  - Parcijalnost dominira nad totalnošću
    - Parcijalna funkcija može pozvati totalnu funkciju, no ne i obrnuto
  - Cilj: maksimalno smanjiti parcijalnost programa

* Instalacija alata i jezika
  - Slobodne implementacije, dostupne za preuzimanje i izmjene
  - Primjer: u Debian GNU/Linuxu
  - Razvojna okruženja
    - Emacs
      : sudo apt-get install emacs
    - Atom: https://atom.io/
** Instalacija Haskella
   : sudo apt-get install haskell-platform
** Instalacija Idrisa
   : sudo apt-get install cabal-install
   : cabal update
   : cabal install idris

* Literatura

  - Članak Davida Turnera: Total Functional Programming
  - Aaron Stump: Verified Functional Programming in Agda, 9. poglavlje (dokazi
    konačnosti)
  - Edwin Brady: Type-driven Development with Idris
  - Daniel Friedman, David Christiansen: The Little Typer

* Zaključak

  - Funkcije kao apstrakcijske komponente
  - Povezivanje manjih funkcija u veće
  - Totalnost: konačne i produktivne funkcije
  - Algebarsko rasuđivanje o totalnim funkcijama
  - Definiranje funkcija na cijeloj domeni
    - Iscrpno podudaranje uzoraka
    - Prilagodba domene ili kodomene
  - Kompilator kao verifikacijski alat

* Poveznice
  - Haskell: https://www.haskell.org/
  - Idris: https://www.idris-lang.org/
  - Atom: https://atom.io/
  - Prezentacija: https://gitlab.com/mdimjasevic/predavanje-totalne-funkcije
  - © Marko Dimjašević, 2018. CC-BY-SA 4.0
    - https://dimjasevic.net/marko
