module RunFuel

%default total

data Fuel = Dry | More (Lazy Fuel)

partial
forever : Fuel
forever = More forever

data InfIO : Type where
  Do : IO a -> (a -> Inf InfIO) -> InfIO

infProg : InfIO
infProg = Do (putStrLn "Lambda") (\_ => infProg)

run : Fuel -> InfIO -> IO ()
run (More fuel) (Do c f) = do res <- c
                              run fuel (f res)
run Dry _ = putStrLn "Out of fuel"

partial
main : IO ()
main = run forever infProg
