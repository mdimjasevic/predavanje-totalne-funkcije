module Greet

data Vehicle = Car | Motorcycle

limit : Nat
limit = 18

agePredicate : Nat -> Bool
agePredicate n = n < limit

possiblyVehicle : {p : Nat -> Bool} -> Nat -> Type
possiblyVehicle {p} n = if p n then () else Vehicle

data Person : (a : Nat) -> Bool -> Type where
  MkPerson
    :  (age : Nat)
    -> (v : possiblyVehicle {p = agePredicate} age)
    -> Person age (agePredicate age)

greet : {age : Nat} -> Person age (agePredicate age) -> String
greet (MkPerson age v) with (agePredicate age)
  greet (MkPerson _ ()) | True =
    "Be patient, you're not old enough to drive!"
  greet (MkPerson _ Car) | False =
    "Hello, you car driver!"
  greet (MkPerson _ Motorcycle) | False =
    "Hello, you motorcycle driver!"
